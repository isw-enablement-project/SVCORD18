package com.isw.svcord18.domain.svord.command;


import org.springframework.stereotype.Service;
import com.isw.svcord18.sdk.domain.svord.command.Svcord18CommandBase;
import com.isw.svcord18.sdk.domain.svord.entity.Svcord18Entity;
import com.isw.svcord18.sdk.domain.svord.entity.ThirdPartyReferenceEntity;
import com.isw.svcord18.sdk.domain.svord.type.ServicingOrderType;
import com.isw.svcord18.sdk.domain.svord.type.ServicingOrderWorkProduct;
import com.isw.svcord18.sdk.domain.svord.type.ServicingOrderWorkResult;
import com.isw.svcord18.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord18.sdk.domain.facade.Repository;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
public class Svcord18Command extends Svcord18CommandBase {

  private static final Logger log = LoggerFactory.getLogger(Svcord18Command.class);

  public Svcord18Command(DomainEntityBuilder entityBuilder,  Repository repo ) {
    super(entityBuilder,  repo );
  }

    @Override
    public com.isw.svcord18.sdk.domain.svord.entity.Svcord18 createServicingOrderProducer(com.isw.svcord18.sdk.domain.svord.entity.CreateServicingOrderProducerInput createServicingOrderProducerInput)  {
    

      log.info("Svcord18Command.createServicingOrderProducer()");
      // TODO: Add your command implementation logic
      Svcord18Entity servicingOrderProcedure = this.entityBuilder.getSvord().getSvcord18().build();
      servicingOrderProcedure.setCustomerReference(createServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessStartDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderType(ServicingOrderType.PAYMENT_CASH_WITHDRAWALS);
      servicingOrderProcedure.setServicingOrderWorkDescription("CASH WITHDRAWALS");
      servicingOrderProcedure.setServicingOrderWorkProduct(ServicingOrderWorkProduct.PAYMENT);
      servicingOrderProcedure.setServicingOrderWorkResult(ServicingOrderWorkResult.PROCESSING);
      
      ThirdPartyReferenceEntity thirdPartyReference = new ThirdPartyReferenceEntity();
      thirdPartyReference.setId("test1");
      thirdPartyReference.setPassword("password");
      servicingOrderProcedure.setThirdPartyReference(thirdPartyReference);

      return repo.getSvord().getSvcord18().save(servicingOrderProcedure);

    }
  
    
    @Override
    public void updateServicingOrderProducer(com.isw.svcord18.sdk.domain.svord.entity.Svcord18 instance, com.isw.svcord18.sdk.domain.svord.entity.UpdateServicingOrderProducerInput updateServicingOrderProducerInput)  { 

      log.info("Svcord18Command.updateServicingOrderProducer()");
      // TODO: Add your command implementation logic
      
      Svcord18Entity servicingOrderProcedure = this.repo.getSvord().getSvcord18().getReferenceById(Long.parseLong(updateServicingOrderProducerInput.getUpdateID()));
      servicingOrderProcedure.setCustomerReference(updateServicingOrderProducerInput.getCustomerReference());
      servicingOrderProcedure.setProcessEndDate(LocalDate.now());
      servicingOrderProcedure.setServicingOrderWorkResult(updateServicingOrderProducerInput.getServicingOrderWorkResult());
      servicingOrderProcedure.setThirdPartyReference(updateServicingOrderProducerInput.getThirdPartyReference());

      log.info(updateServicingOrderProducerInput.getUpdateID().toString());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAccountNumber());
      log.info(updateServicingOrderProducerInput.getCustomerReference().getAmount());

      this.repo.getSvord().getSvcord18().save(servicingOrderProcedure);



    }
  
}
