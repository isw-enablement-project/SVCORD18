package com.isw.svcord18.domain.svord.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.sleuth.annotation.NewSpan;
import org.springframework.stereotype.Service;

import com.isw.svcord18.sdk.domain.svord.entity.CreateServicingOrderProducerInput;
import com.isw.svcord18.sdk.domain.svord.entity.CustomerReferenceEntity;
import com.isw.svcord18.sdk.domain.svord.entity.Svcord18;
import com.isw.svcord18.sdk.domain.svord.entity.UpdateServicingOrderProducerInput;
import com.isw.svcord18.sdk.domain.svord.entity.WithdrawalDomainServiceOutput;
import com.isw.svcord18.sdk.domain.svord.service.WithdrawalDomainServiceBase;
import com.isw.svcord18.sdk.domain.svord.type.ServicingOrderWorkResult;
import com.isw.svcord18.sdk.integration.partylife.entity.RetrieveLoginInput;
import com.isw.svcord18.sdk.integration.partylife.entity.RetrieveLoginOutput;
import com.isw.svcord18.sdk.integration.paymord.entity.PaymentOrderInput;
import com.isw.svcord18.sdk.integration.paymord.entity.PaymentOrderOutput;
import com.isw.svcord18.domain.svord.command.Svcord18Command;
import com.isw.svcord18.integration.partylife.service.RetrieveLogin;
import com.isw.svcord18.integration.paymord.service.PaymentOrder;
import com.isw.svcord18.sdk.domain.facade.DomainEntityBuilder;
import com.isw.svcord18.sdk.domain.facade.Repository;
import com.isw.svcord18.sdk.integration.facade.IntegrationEntityBuilder;

@Service
public class WithdrawalDomainService extends WithdrawalDomainServiceBase {

  private static final Logger log = LoggerFactory.getLogger(WithdrawalDomainService.class);

  @Autowired
  Svcord18Command servicingOrderProcedureCommand;

  @Autowired
  PaymentOrder paymentOrder;

  @Autowired
  RetrieveLogin retrieveLogin;

  @Autowired
  IntegrationEntityBuilder integrationEntityBuilder;

  public WithdrawalDomainService(DomainEntityBuilder entityBuilder,  Repository repo) {
  
    super(entityBuilder,  repo);
    
  }
  
  @NewSpan
  @Override
  public com.isw.svcord18.sdk.domain.svord.entity.WithdrawalDomainServiceOutput execute(com.isw.svcord18.sdk.domain.svord.entity.WithdrawalDomainServiceInput withdrawalDomainServiceInput)  {

    log.info("WithdrawalDomainService.execute()");
  
  
    RetrieveLoginInput retrieveInput = integrationEntityBuilder
    .getPartylife().getRetrieveLoginInput().setId("test1").build();
    

    RetrieveLoginOutput retriveSerivceOutput = retrieveLogin.execute(retrieveInput);
    
    if(retriveSerivceOutput.getResult() != "SUCCESS") {
      return null;
    }

    // Rootentity servicing order 생성
    CustomerReferenceEntity customerReference = this.entityBuilder.getSvord()
    .getCustomerReference().build();

    customerReference.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    customerReference.setAmount(withdrawalDomainServiceInput.getAmount());

    CreateServicingOrderProducerInput createInput = this.entityBuilder.getSvord()
    .getCreateServicingOrderProducerInput().build();

    createInput.setCustomerReference(customerReference);
    Svcord18 createOutput = servicingOrderProcedureCommand.createServicingOrderProducer(createInput);


    //payment order호출
    PaymentOrderInput paymentOrderInput = integrationEntityBuilder.getPaymord().getPaymentOrderInput().build();
    paymentOrderInput.setAccountNumber(withdrawalDomainServiceInput.getAccountNumber());
    paymentOrderInput.setAmount(withdrawalDomainServiceInput.getAmount());
    paymentOrderInput.setExternalId("SVCORD18");
    paymentOrderInput.setExternalSerive("SVCORD18");
    paymentOrderInput.setPaymentType("CASH_WITHDRAWAL");

    PaymentOrderOutput paymentOrderOutput = paymentOrder.execute(paymentOrderInput);

    //pyment order 결과 updates
    UpdateServicingOrderProducerInput updateServicingOrderProducerInput = this.entityBuilder.getSvord()
    .getUpdateServicingOrderProducerInput().build();
    updateServicingOrderProducerInput.setUpdateID(createOutput.getId().toString());
    updateServicingOrderProducerInput
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()));

    servicingOrderProcedureCommand.updateServicingOrderProducer(createOutput, updateServicingOrderProducerInput);

    WithdrawalDomainServiceOutput withdrawalDomainServiceOutput = this.entityBuilder.getSvord().getWithdrawalDomainServiceOutput()
    .setServicingOrderWorkResult(ServicingOrderWorkResult.valueOf(paymentOrderOutput.getPaymentOrderReulst()))
    .setTrasactionId(paymentOrderOutput.getTransactionId())
    .build();

    return withdrawalDomainServiceOutput;

    
  }

}
